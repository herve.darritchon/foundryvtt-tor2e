import { changeCommunityValueByLabel, createCommunityActor, dragndropAWeaonItem, communityCleanUp } from '../tests/utils/helpers/community-utils.mjs';
import { createCharacterActor, characterCleanUp } from '../tests/utils/helpers/character-utils.mjs';
import { createWeaponItem } from '../tests/utils/helpers/weapon-utils.mjs';
import { GAMEMASTER } from '../tests/utils/user-utils.mjs';
import { initialiseTests } from '../tests/utils/helpers/foundry-vtt.mjs';

// @ts-check
const { test, expect } = require('@playwright/test');

test.describe('Add a simple actor tests', () => {
  test.describe.configure({ mode: 'serial' });


  test('create a community actor and fill it', async ({ page }) => {
    // GIVEN
    test.setTimeout(20000);

    await initialiseTests({ page, user: GAMEMASTER });

    // WHEN
    const community = {
      name: 'Default Community'
    };

    await createCommunityActor({ page, community });

    // THEN
    await expect(page.getByLabel('Fellowship Points')).toHaveValue('2');
    await expect(page.getByLabel('Max')).toHaveValue('3');
    await expect(page.getByLabel('Eye Awareness')).toHaveValue('1');
    await expect(page.getByLabel('Initial Value')).toHaveValue('2');

    // FINALLY
    await communityCleanUp({ page, community });
  });

  test('create a community actor and change one value', async ({ page }) => {
    // GIVEN
    test.setTimeout(20000);

    await initialiseTests({ page, user: GAMEMASTER });

    // WHEN
    const community = {
      name: 'Community With Change Value'
    };

    await createCommunityActor({ page, community });
    await changeCommunityValueByLabel({ page, label: 'Fellowship Points', value: '3' });

    // THEN
    await expect(page.getByLabel('Fellowship Points')).toHaveValue('3');

    // FINALLY
    await communityCleanUp({ page, community });
  });

  test('create a character actor and fill biography', async ({ page }) => {
    // GIVEN
    test.setTimeout(10000);

    await initialiseTests({ page, user: GAMEMASTER });

    // WHEN
    const character = {
      name: 'Actor with Biography',
      blocks: ['biography']
    };

    await createCharacterActor({ page, character });

    // THEN
    await expect(page.getByPlaceholder('Age')).toHaveValue('25');
    await expect(page.getByLabel('Title')).toHaveValue('Some Title');
    await expect(page.getByLabel('Fellowship Focus')).toHaveValue('Some Fellowship Focus');
    await expect(page.getByLabel('Culture')).toHaveValue('Some Culture');
    await expect(page.getByLabel('Standard of Living')).toHaveValue('prosperous');
    await expect(page.getByLabel('Cultural Blessing')).toHaveValue('Some Cultural Blessing');
    await expect(page.getByLabel('Calling')).toHaveValue('treasure-hunter');
    await expect(page.getByLabel('Shadow Path')).toHaveValue('Some Shadow Path');

    // FINALLY
    await characterCleanUp({ page, character });
  });

  test('create a character actor and fill heroic stature', async ({ page }) => {
    // GIVEN
    test.setTimeout(10000);

    await initialiseTests({ page, user: GAMEMASTER });

    // WHEN
    const character = {
      name: 'Actor with Heroic Stature',
      blocks: ['heroic stature']
    };

    await createCharacterActor({ page, character });

    // THEN
    await expect(page.getByLabel('Valour')).toHaveValue('3');
    await expect(page.getByLabel('Wisdom')).toHaveValue('2');
    await expect(page.getByTitle('Fear')).toHaveClass(/favoured-attribute/);
    await expect(page.getByTitle('Corruption')).toHaveClass(/favoured-attribute/);

    // FINALLY
    await characterCleanUp({ page, character });
  });

  test('create a character actor and fill stats', async ({ page }) => {
    // GIVEN
    test.setTimeout(12000);

    await initialiseTests({ page, user: GAMEMASTER });

    // WHEN
    const character = {
      name: 'Actor with Stats',
      blocks: ['stats']
    };

    await createCharacterActor({ page, character });

    // THEN
    await expect(page.locator('[id="system\\.attributes\\.strength\\.value"]')).toHaveValue('7');
    await expect(page.locator('[id="system\\.attributes\\.heart\\.value"]')).toHaveValue('4');
    await expect(page.locator('[id="system\\.attributes\\.wits\\.value"]')).toHaveValue('5');
    await expect(page.locator('#strength-tn')).toHaveText('13');
    await expect(page.locator('#heart-tn')).toHaveText('16');
    await expect(page.locator('#wits-tn')).toHaveText('15');

    // FINALLY
    await characterCleanUp({ page, character });
  });

  test('create a character actor and fill resources', async ({ page }) => {
    // GIVEN
    test.setTimeout(20000);

    await initialiseTests({ page, user: GAMEMASTER });

    // WHEN
    const character = {
      name: 'Actor with Resources',
      blocks: ['resources']
    };

    await createCharacterActor({ page, character });

    // THEN
    await expect(page.locator('[id="system\\.resources\\.endurance\\.max"]')).toHaveValue('5');
    await expect(page.getByLabel('Endurance')).toHaveValue('25');
    await expect(page.getByLabel('Fatigue')).toHaveValue('7');
    await expect(page.locator('#load')).toHaveText('7');
    await expect(page.locator('[id="system\\.resources\\.hope\\.max"]')).toHaveValue('15');
    await expect(page.getByLabel('Hope')).toHaveValue('14');
    await expect(page.locator('[id="system\\.resources\\.hope\\.max"]')).toHaveValue('15');
    await expect(page.locator('#shadow')).toHaveText('2');
    await expect(page.getByLabel('Temporary')).toHaveValue('1');
    await expect(page.getByLabel('Shadow Scars')).toHaveValue('1');

    // FINALLY
    await characterCleanUp({ page, character });
  });

  test('create a weapon item and fill it', async ({ page }) => {
    // GIVEN
    test.setTimeout(20000);

    await initialiseTests({ page, user: GAMEMASTER });

    // WHEN
    const weapon = {
      name: 'Default Weapon'
    };
    await createWeaponItem({ page, weapon });

    // THEN

  });

  test('drag\'n drop a weapon item into a character sheet', async ({ page }) => {
    // GIVEN
    test.setTimeout(20000);

    await initialiseTests({ page, user: GAMEMASTER });

    // WHEN
    const actorName = 'Actor for Drag\'n Drop';
    const weaponName = 'A Sword for Drag\'n Drop';

    const character = {
      name: actorName,
      blocks: ['biography']
    };

    await createCharacterActor({ page, character });
    
    const weapon = {
      name: weaponName
    };

    await createWeaponItem({ page, weapon });

    //await dragndropAWeaonItem({ page, actor: { label: actorName }, item: { name: weaponName } });

    // THEN
  });

});