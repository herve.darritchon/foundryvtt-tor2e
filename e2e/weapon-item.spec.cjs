import { dragndropAWeaonItem } from '../tests/utils/helpers/community-utils.mjs';
import { createCharacterActor } from '../tests/utils/helpers/character-utils.mjs';
import { createWeaponItem } from '../tests/utils/helpers/weapon-utils.mjs';
import { GAMEMASTER } from '../tests/utils/user-utils.mjs';
import { initialiseTests } from '../tests/utils/helpers/foundry-vtt.mjs';

// @ts-check
const { test, expect } = require('@playwright/test');

test.describe('weapon item tests', () => {
  test.describe.configure({ mode: 'serial' });

  test('create a weapon item and fill it', async ({ page }) => {
    // GIVEN
    test.setTimeout(20000);

    await initialiseTests({ page, user: GAMEMASTER });

    // WHEN
    const weapon = {
      name: 'Default Weapon'
    };
    await createWeaponItem({ page, weapon });

    // THEN

  });

  test('drag\'n drop a weapon item into a character sheet', async ({ page }) => {
    // GIVEN
    test.setTimeout(20000);

    await initialiseTests({ page, user: GAMEMASTER });

    // WHEN
    const actorName = 'Actor for Drag\'n Drop';
    const weaponName = 'A brand new sword';

    const character = {
      name: actorName
    };
    await createCharacterActor({ page, character });

    const weapon = {
      name: 'Default Weapon'
    };
    await createWeaponItem({ page, weapon });

    await dragndropAWeaonItem({ page, actor: { label: actorName }, item: { name: weaponName } });

    // THEN
  });

});