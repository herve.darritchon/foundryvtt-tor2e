import { changeCommunityValueByLabel, createCommunityActor, communityCleanUp } from '../tests/utils/helpers/community-utils.mjs';
import { GAMEMASTER } from '../tests/utils/user-utils.mjs';
import { initialiseTests } from '../tests/utils/helpers/foundry-vtt.mjs';

// @ts-check
const { test, expect } = require('@playwright/test');

test.describe('community actor tests', () => {
  test.describe.configure({ mode: 'serial' });


  test('create a community actor and fill it', async ({ page }) => {
    // GIVEN
    test.setTimeout(20000);

    await initialiseTests({ page, user: GAMEMASTER });

    // WHEN
    const community = {
      name: 'Default Community'
    };

    await createCommunityActor({ page, community });

    // THEN
    await expect(page.getByLabel('Fellowship Points')).toHaveValue('2');
    await expect(page.getByLabel('Max')).toHaveValue('3');
    await expect(page.getByLabel('Eye Awareness')).toHaveValue('1');
    await expect(page.getByLabel('Initial Value')).toHaveValue('2');

    // FINALLY
    await communityCleanUp({ page, community });
  });

  test('create a community actor and change one value', async ({ page }) => {
    // GIVEN
    test.setTimeout(20000);

    await initialiseTests({ page, user: GAMEMASTER });

    // WHEN
    const community = {
      name: 'Community With Change Value'
    };

    await createCommunityActor({ page, community });
    await changeCommunityValueByLabel({ page, label: 'Fellowship Points', value: '3' });

    // THEN
    await expect(page.getByLabel('Fellowship Points')).toHaveValue('3');

    // FINALLY
    await communityCleanUp({ page, community });
  });

});