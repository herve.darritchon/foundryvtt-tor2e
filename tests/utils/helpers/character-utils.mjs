const { chromium } = require('playwright');

export async function _createCharacterActor({ page, character }) {
    await page.getByLabel('Actors', { exact: true }).click();
    await page.getByRole('button', { name: /Create Actor/ }).click();
    await page.locator('#notifications i').click();

    await page.getByPlaceholder('New Actor').fill(character.name);
    await page.getByRole('combobox').selectOption('character');
    await page.getByRole('button', { name: /Create New Actor/ }).click();
}

export async function _fillCharacterActorBiography({ page, character }) {
    await page.getByPlaceholder('Name').fill(character.name);
    await page.getByPlaceholder('Age').press('ArrowUp', { delay: 120 });
    await page.getByPlaceholder('Age').press('ArrowUp', { delay: 120 });
    await page.getByPlaceholder('Age').press('ArrowUp', { delay: 120 });
    await page.getByPlaceholder('Age').press('ArrowUp', { delay: 120 });
    await page.getByPlaceholder('Age').press('ArrowUp', { delay: 120 });
    await page.getByPlaceholder('Age').press('ArrowUp', { delay: 120 });
    await page.getByPlaceholder('Age').press('ArrowUp', { delay: 120 });
    await page.getByPlaceholder('Age').press('Tab');
    await page.getByLabel('Title').fill('Some Title');
    await page.getByLabel('Title').press('Tab');
    await page.getByLabel('Fellowship Focus').fill('Some Fellowship Focus');
    await page.getByLabel('Fellowship Focus').press('Tab');

    await page.getByLabel('Culture').fill('Some Culture');
    await page.getByLabel('Culture').press('Tab');
    await page.getByLabel('Standard of Living').selectOption('Prosperous');

    await page.getByLabel('Cultural Blessing').fill('Some Cultural Blessing');
    await page.getByLabel('Calling').selectOption('treasure-hunter');

    await page.getByLabel('Shadow Path').fill('Some Shadow Path');
}

export async function _fillCharacterActorHeroicStature({ page }) {
    await page.getByLabel('Valour').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Valour').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Valour').press('Tab');
    await page.getByLabel('Wisdom').press('ArrowUp', { delay: 120 });
    await page.getByTitle('Fear').click({
        modifiers: ['Alt']
    });
    await page.getByTitle('Corruption').click({
        modifiers: ['Alt']
    });
}

export async function _fillCharacterActorStats({ page }) {
    await page.locator('[id="system\\.attributes\\.strength\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.strength\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.strength\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.strength\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.strength\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.strength\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.strength\\.value"]').press('ArrowUp', { delay: 120 });

    await page.locator('[id="system\\.attributes\\.heart\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.heart\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.heart\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.heart\\.value"]').press('ArrowUp', { delay: 120 });

    await page.locator('[id="system\\.attributes\\.wits\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.wits\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.wits\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.wits\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.wits\\.value"]').press('ArrowUp', { delay: 120 });
}

export async function _fillCharacterActorResources({ page }) {

    await page.locator('[id="system\\.resources\\.endurance\\.max"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.resources\\.endurance\\.max"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.resources\\.endurance\\.max"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.resources\\.endurance\\.max"]').press('ArrowUp', { delay: 120 });

    await page.getByLabel('Endurance').fill('24');
    await page.getByLabel('Endurance').press('ArrowUp', { delay: 120 });

    await page.getByLabel('Fatigue').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Fatigue').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Fatigue').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Fatigue').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Fatigue').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Fatigue').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Fatigue').press('ArrowUp', { delay: 120 });

    await page.locator('[id="system\\.resources\\.hope\\.max"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.resources\\.hope\\.max"]').press('ArrowUp', { delay: 120 });

    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });

    await page.locator('[id="system\\.resources\\.hope\\.max"]').click();
    await page.locator('[id="system\\.resources\\.hope\\.max"]').press('ArrowRight');
    await page.locator('[id="system\\.resources\\.hope\\.max"]').fill('15');

    await page.getByLabel('Temporary').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Temporary').press('ArrowUp', { delay: 120 });

    await page.getByLabel('Shadow Scars').press('ArrowUp', { delay: 120 });

}

export async function _fillCharacterActorMiscellaneous({ page }) {
    await page.getByLabel('Fellowship', { exact: true }).press('ArrowUp', { delay: 120 });
    await page.getByLabel('Fellowship', { exact: true }).press('Tab');
    await page.locator('[id="system\\.skillPoints\\.total\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.skillPoints\\.total\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.skillPoints\\.total\\.value"]').press('Tab');
    await page.getByLabel('Skill Points').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Skill Points').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Skill Points').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Skill Points').press('Tab');
    await page.locator('[id="system\\.adventurePoints\\.total\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.adventurePoints\\.total\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.adventurePoints\\.total\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.adventurePoints\\.total\\.value"]').press('Tab');
    await page.getByLabel('Adventure Points').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Adventure Points').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Adventure Points').press('Tab');
    await page.getByLabel('Treasure').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Treasure').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Treasure').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Treasure').press('Tab');
}

export async function _fillCharacterActor({ page }) {
    await page.getByPlaceholder('New Actor').fill('Actor');
    await page.getByRole('combobox').selectOption('character');
    await page.getByRole('button', { name: ' Create New Actor' }).click();

    await page.getByPlaceholder('Name').fill('New Actor');
    await page.getByPlaceholder('Age').press('ArrowUp', { delay: 120 });
    await page.getByPlaceholder('Age').press('ArrowUp', { delay: 120 });
    await page.getByPlaceholder('Age').press('ArrowUp', { delay: 120 });
    await page.getByPlaceholder('Age').press('ArrowUp', { delay: 120 });
    await page.getByPlaceholder('Age').press('ArrowUp', { delay: 120 });
    await page.getByPlaceholder('Age').press('ArrowUp', { delay: 120 });
    await page.getByPlaceholder('Age').press('ArrowUp', { delay: 120 });
    await page.getByPlaceholder('Age').press('Tab');
    await page.getByLabel('Title').fill('Some Title');
    await page.getByLabel('Title').press('Tab');
    await page.getByLabel('Fellowship Focus').fill('Some Focus');
    await page.getByLabel('Fellowship Focus').press('Tab');

    await page.getByLabel('Culture').fill('Hobbit of the Shire');
    await page.getByLabel('Culture').press('Tab');
    await page.getByLabel('Standard of Living').selectOption('common');

    await page.getByLabel('Cultural Blessing').fill('Stealthy');
    await page.getByLabel('Calling').selectOption('treasure-hunter');

    await page.getByLabel('Shadow Path').fill('Greedy');

    await page.getByLabel('Valour').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Valour').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Valour').press('Tab');
    await page.getByLabel('Wisdom').press('ArrowUp', { delay: 120 });
    await page.getByTitle('Fear').click({
        modifiers: ['Alt']
    });
    await page.getByTitle('Corruption').click({
        modifiers: ['Alt']
    });

    await page.locator('[id="system\\.attributes\\.strength\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.strength\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.strength\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.strength\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.strength\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.strength\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.strength\\.value"]').press('ArrowUp', { delay: 120 });

    await page.locator('[id="system\\.attributes\\.heart\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.heart\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.heart\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.heart\\.value"]').press('ArrowUp', { delay: 120 });

    await page.locator('[id="system\\.attributes\\.wits\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.wits\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.wits\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.wits\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.attributes\\.wits\\.value"]').press('ArrowUp', { delay: 120 });

    await page.locator('[id="system\\.resources\\.endurance\\.max"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.resources\\.endurance\\.max"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.resources\\.endurance\\.max"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.resources\\.endurance\\.max"]').press('ArrowUp', { delay: 120 });

    await page.locator('[id="system\\.resources\\.endurance\\.max"]').press('ArrowRight');
    await page.locator('[id="system\\.resources\\.endurance\\.max"]').fill('28');

    await page.getByLabel('Endurance').press('ArrowRight');
    await page.getByLabel('Endurance').fill('24');

    await page.getByLabel('Fatigue').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Fatigue').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Fatigue').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Fatigue').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Fatigue').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Fatigue').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Fatigue').press('ArrowUp', { delay: 120 });

    await page.locator('[id="system\\.resources\\.hope\\.max"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.resources\\.hope\\.max"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.resources\\.hope\\.max"]').press('Tab');
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Hope').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.resources\\.hope\\.max"]').click();
    await page.locator('[id="system\\.resources\\.hope\\.max"]').press('ArrowRight');
    await page.locator('[id="system\\.resources\\.hope\\.max"]').fill('15');
    await page.locator('[id="system\\.resources\\.hope\\.max"]').press('Tab');
    await page.getByLabel('Hope').press('Tab');
    await page.getByLabel('Temporary').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Temporary').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Temporary').press('Tab');
    await page.getByLabel('Shadow Scars').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Shadow Scars').press('Tab');
    await page.getByLabel('Parry').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Parry').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Parry').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Parry').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Parry').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Parry').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Parry').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Parry').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Parry').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Parry').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Parry').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Parry').press('ArrowDown');
    await page.getByLabel('Parry').press('ArrowDown');
    await page.getByLabel('Parry').press('ArrowDown');
    await page.getByLabel('Parry').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Parry').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Parry').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Parry').press('ArrowDown');
    await page.getByLabel('Parry').press('Tab');
    await page.getByLabel('Fellowship', { exact: true }).press('ArrowUp', { delay: 120 });

    await page.locator('[id="system\\.skillPoints\\.total\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.skillPoints\\.total\\.value"]').press('ArrowUp', { delay: 120 });

    await page.getByLabel('Skill Points').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Skill Points').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Skill Points').press('ArrowUp', { delay: 120 });

    await page.locator('[id="system\\.adventurePoints\\.total\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.adventurePoints\\.total\\.value"]').press('ArrowUp', { delay: 120 });
    await page.locator('[id="system\\.adventurePoints\\.total\\.value"]').press('ArrowUp', { delay: 120 });

    await page.getByLabel('Adventure Points').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Adventure Points').press('ArrowUp', { delay: 120 });

    await page.getByLabel('Treasure').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Treasure').press('ArrowUp', { delay: 120 });
    await page.getByLabel('Treasure').press('ArrowUp', { delay: 120 });

}

export async function createCharacterActor({ page, character }) {
    await _createCharacterActor({ page, character });

    if (character.blocks.includes('biography')) {
        await _fillCharacterActorBiography({ page, character });
    }
    if (character.blocks.includes('heroic stature')) {
        await _fillCharacterActorHeroicStature({ page });
    }
    if (character.blocks.includes('stats')) {
        await _fillCharacterActorStats({ page });
    }
    if (character.blocks.includes('resources')) {
        await _fillCharacterActorResources({ page });
    }
    if (character.blocks.includes('miscellaneous')) {
        await _fillCharacterActorMiscellaneous({ page });
    }
}

export async function characterCleanUp({ page, character }) {
    await page.evaluate((character) => {
        const ids = game.actors.filter(actor => actor.name === character.name).map(actor => actor.id)
        Actor.deleteDocuments(ids);
    }, character);
}