const { test, expect } = require('@playwright/test');
const { chromium } = require('playwright');

async function _handleAuthPage({ page }) {
    // Expects page to be on the auth page.
    await expect(page).toHaveURL(/auth/);
    await expect(page).toHaveTitle(/Foundry Virtual Tabletop/);

    // Text input
    await page.locator('#key').fill('Aotearoa3"');

    // Click the get started link.
    await page.getByRole('button').click();

    // Expects page to have a heading with the name of Installation.
    await expect(page.getByRole('heading', { name: 'Foundry Virtual Tabletop' })).toBeVisible();
    await expect(page.locator('#setup')).toBeVisible();
}

async function _logOutFromGame({ page }) {
    await page.getByLabel('Game Settings').click();
    await page.getByRole('button', { name: /Return to Setup/ }).click();
}

async function _connectAsA({ page, user }) {
    const gamemaster = await page.getByRole('combobox').selectOption({ label: user.role });
    await page.getByRole('button', { name: /Join Game Session/ }).click();
}

async function _conectTor2eWorld({ page }) {
    // Clear notif and popin
    await page.locator('#notifications i').click();
    
    // Connect to TOR2e game
    const link = page.locator('li').filter({ hasText: /tor2e-v12/ });

    await link.hover();
    await link.locator('a').click();
}

async function _conectAdministrator({ page }) {
    await page.getByPlaceholder('Administrator Password').click();
    await page.getByPlaceholder('Administrator Password').fill('Aotearoa3"');
    await page.getByRole('button', { name: /Log In/ }).click();
}

async function _returnToSetupPageFromWorldLogInPage({ page }) {
    await page.getByPlaceholder('Administrator Password').click();
    await page.getByPlaceholder('Administrator Password').fill('Aotearoa3"');
    await page.getByRole('button', { name: /Return to Setup/ }).click();
}

async function _returnToAuthPageFromSeupPagePage({ page }) {
    // Log out
    await page.getByLabel('Log Out').click();
}

export async function worldSelectionPage({ page }) {
    await page.goto('http://localhost:30000/auth');

    await _handleAuthPage({ page });
}

export async function connectInAGame({ page, user }) {

    if (page.url().includes('auth')) {
        await _conectAdministrator({ page });
    }
    if (page.url().includes('setup')) {
        await _conectTor2eWorld({ page });
        await _connectAsA({ page, user });
    }

    if (page.url().includes('join')) {
        await _connectAsA({ page, user });
    }
    if (page.url().includes('game')) {
        await _logOutFromGame({ page });
        await _connectAsA({ page, user });
    }
}

export async function fromLogInPageToSetupAuthPage({ page }) {

    await _returnToSetupPageFromWorldLogInPage({ page });
    await _returnToAuthPageFromSeupPagePage({ page });
}

export async function logOut({ page }) {

    if (page.url().includes('game')) {
        await _logOutFromGame({ page });
    }

    if (page.url().includes('join')) {
        await _returnToSetupPageFromWorldLogInPage({ page });
    }

    if (page.url().includes('setup')) {
        await _returnToAuthPageFromSeupPagePage({ page });
    }
}

export async function initialiseTests({ page, user }) {
    await page.goto('http://localhost:30000/');

    let storage = await page.evaluate(() => window.localStorage);

    await page.evaluate((val) => localStorage.setItem('core.tourProgress', val), '{"core":{"backupsOverview":0}}');

    storage = await page.evaluate(() => window.localStorage);

    await connectInAGame({ page, user });

    await expect(page).toHaveURL(/game/);
    await expect(page).toHaveTitle(/Foundry Virtual Tabletop/);
    const body = page.locator('body');
    await expect(body).toHaveClass(/system-tor2e/);

}