export async function changeWeaponValueByLabel({ page, label, value }) {
    await page.getByLabel(label).fill(value);
}

export async function createWeaponItem({ page, weapon }) {
    await page.getByLabel('Items', { exact: true }).click();
    await page.getByRole('button', { name: ' Create Item' }).click();;
    await page.getByPlaceholder('New Item').fill(weapon.name);
    //await page.getByRole('combobox').selectOption('weapon');
    await page.locator('select[name="type"]').selectOption('weapon');
    await page.getByRole('button', { name: ' Create New Item' }).click();
    await page.getByLabel('Damage').click();
    await page.getByLabel('Damage').press('ArrowUp');
    await page.getByLabel('Damage').press('ArrowUp');
    await page.getByLabel('Damage').press('ArrowUp');
    await page.getByLabel('Damage').press('ArrowUp');
    await page.getByLabel('Damage').press('ArrowUp');
    await page.locator('form').filter({ hasText: 'Weapon Details Effects Damage' }).locator('span').nth(1).click();
    await page.getByLabel('Injury').click();
    await page.getByLabel('Injury').press('ArrowUp');
    await page.getByLabel('Injury').press('ArrowUp');
    await page.getByLabel('Injury').press('ArrowUp');
    await page.getByLabel('Injury').press('ArrowUp');
    await page.getByLabel('Injury').press('ArrowUp');
    await page.getByLabel('Injury').press('ArrowUp');
    await page.getByLabel('Load').click();
    await page.getByLabel('Load').press('ArrowUp');
    await page.getByLabel('Load').press('ArrowUp');
    await page.getByLabel('Group').selectOption('swords');
    await page.getByLabel('Two Hands').check();
    await page.getByLabel('Notes', { exact: true }).click();
    await page.getByLabel('Notes', { exact: true }).fill('A splendid sword');
    const description = page.locator('#description div').first();
    await description.hover();
    await page.locator('#description a').hover();
    await page.mouse.down();
    await page.mouse.up();
    
    await page.getByRole('img', { name: 'Image representing the Weapon item.' }).click();
    await page.getByText('Core Data').click();
    await page.getByText('icons').click();
    await page.getByText('weapons').click();
    await page.getByText('swords', { exact: true }).click();
    await page.getByText('greatsword-crossguard-blue.').click();
    await page.getByRole('button', { name: ' Select File' }).click();
    await page.getByRole('img', { name: 'Image representing the Weapon item.' }).click();
    await page.getByText('greatsword-crossguard-flanged-red.webp').click();
    await page.getByRole('button', { name: ' Select File' }).click();
 
}