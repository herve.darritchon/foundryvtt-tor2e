const { test, expect } = require('@playwright/test');
const { chromium } = require('playwright');

export async function makeCommunityActorDefault({ page }) {

  await page.getByRole('img', { name: 'to make this community actor' }).click({ modifiers: ['Alt'] });

}

export async function changeCommunityValueByLabel({ page, label, value }) {
  await page.getByLabel(label).fill(value);
}

export async function dragndropAWeaonItem({ page, actor, item }) {
  await page.getByLabel('Actors', { exact: true }).click();
  await page.getByText(actor.label).first().click();

  await page.getByLabel('Items', { exact: true }).click();
  await page.getByRole('heading', { name: item.name, exact: true }).dragTo(page.getByRole('heading', { name: 'Biography' }));
}

export async function createCommunityActor({ page, community }) {

  await page.locator('#notifications i').click();
  await page.getByLabel('Actors', { exact: true }).click();
  await page.getByRole('button', { name: ' Create Actor' }).click();

  await page.getByPlaceholder('New Actor').fill(community.name);
  await page.getByRole('button', { name: /Create New Actor/ }).click();

  await page.getByLabel('Fellowship Points').press('ArrowUp', { delay: 250 });
  await page.getByLabel('Fellowship Points').press('ArrowUp', { delay: 250 });
  await page.getByLabel('Fellowship Points').press('ArrowUp', { delay: 250 });
  await page.getByLabel('Fellowship Points').press('ArrowDown', { delay: 250 });

  await page.getByLabel('Max').press('ArrowUp', { delay: 250 });
  await page.getByLabel('Max').press('ArrowUp', { delay: 250 });
  await page.getByLabel('Max').press('ArrowUp', { delay: 250 });

  await page.getByLabel('Eye Awareness').press('ArrowUp', { delay: 250 });

  await page.getByLabel('Initial Value').press('ArrowUp', { delay: 250 });
  await page.getByLabel('Initial Value').press('ArrowUp', { delay: 250 });

}

export async function communityCleanUp({ page, community }) {
  await page.evaluate((community) => {
    ids = game.actors.filter(actor => actor.name === community.name).map(actor => actor.id)
    Actor.deleteDocuments(ids);
  }, community);
}