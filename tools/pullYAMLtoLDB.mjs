import {compilePack} from '@foundryvtt/foundryvtt-cli';
import {promises as fs} from 'fs';

const MODULE_ID = process.cwd();
const yaml = true;
const log = true;

//const packs = await fs.readdir('./src/compendiums').filter(f => f.endsWith('.DS_Store'));
const packs = (await fs.readdir('./src/compendiums')).filter(f => f !== '.DS_Store');
for (const pack of packs) {
    if (pack === '.gitattributes') continue;
    console.log('Packing ' + pack);
    await compilePack(
        `${MODULE_ID}/src/compendiums/${pack}`,
        `${MODULE_ID}/src/packs/${pack}`,
        {yaml, log}
    );
}