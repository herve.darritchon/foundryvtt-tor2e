import {taskCheck} from "./sheets/dice.js";
import {tor2eUtilities} from "./utilities.js";
import {Tor2eChatMessage} from "./chat/Tor2eChatMessage.js";

export default class Tor2eSong {

    constructor({songId, user, community}) {
        this.songId = songId;
        this.user = user;
        this.community = community;
    }

    async selectAPlayerCharacter() {
        if (this.user.isGM) {
            ui.notifications.info("GM, select the player to play the song from the list of players");
            await this._selectAPlayerCharacterAsAGM();
        } else {
            const actor = game.actors.get(game.user.character.id);

            if (actor == null) {
                ui.notifications.error(game.i18n.localize(`tor2e.actors.community.messages.noCharacterAttachedToUser ${user.name}`));
                return;
            }
            this.actor = actor;
        }
    }

    async _selectAPlayerCharacterAsAGM() {
        const players = this.community.system.members
        const playersList = players.map(player => {
            return {
                id: player.id,
                name: player.name,
                avatar: player.token
            };
        });

        return new Promise(async (resolve, reject) => {
            const dialog = new Dialog({
                title: game.i18n.localize("tor2e.actors.community.messages.selectPlayer"),
                content: await renderTemplate("systems/tor2e/templates/dialog/select-player-dialog.hbs", {players: playersList}),
                buttons: {
                    play: {
                        icon: '<i class="fa fa-check"></i>',
                        label: game.i18n.localize("tor2e.actors.community.messages.select-a-player"),
                        callback: async html => {
                            const form = html[0].querySelector("form");
                            const fd = new FormDataExtended(form);
                            const actor = this._getSelectedPlayerFrom(fd.object.actorId);
                            if (actor) {
                                this.actor = actor;
                                resolve(actor);
                            } else {
                                reject(new Error("No actor selected"));
                            }
                        },
                    }
                },
                default: "play",
                close: () => reject(new Error("Dialog closed without selection"))
            });

            dialog.render(true);
        });
    }

    _getSelectedPlayerFrom(actorId) {
        let actor = game.actors.get(actorId);

        if (actor == null) {
            ui.notifications.error(game.i18n.localize("tor2e.actors.community.messages.noCharacterAttachedToUser"));
            return;
        }
        return actor;
    }

    async _getSongFromId() {
        let song = this.community.items.get(this.songId);

        if (!song) {
            ui.notifications.error(game.i18n.localize(`tor2e.actors.community.messages.unknownSong with id: ${songId}`));
            return;
        }
        this.song = song;
        return song;
    }

    async play() {
        await this.selectAPlayerCharacter();
        await this._getSongFromId();
        await this._playSong();
    }

    async _playSong() {
        let song = this.song;
        let actor = this.actor;
        const tn = actor.extendedData.getStrengthTn();

        let optionData = {
            song: song,
            difficulty: tn,
            user: this.user,
            actor: actor,
        }

        let roll = await taskCheck({
            actor: actor,
            user: this.user,
            difficulty: tn,
            askForOptions: true,
            actionValue: actor.system.commonSkills.song.value,
            actionName: song.name,
            wearyRoll: actor.getWeary(),
            miserableRoll: actor.getMiserable(),
            modifier: 0,
            shadowServant: false,
            hopePoint: tor2eUtilities.utilities.try(() => actor.system.resources.hope.value, 0),
            favouredRoll: actor.system.commonSkills.song.favoured.value,
            illFavouredRoll: actor.getIllFavoured(),
        });

        if (!roll || (roll?.customResult?.result?.type?.isFailure ?? true)) {
            return;
        }

        await this._createSongResultChatMessage(roll, optionData);
    }

    async _createSongResultChatMessage(roll, options) {
        let chatData = {
            user: options.user.id,
            speaker: ChatMessage.getSpeaker()
        };

        const name = options.actor.name;
        let cardData = {
            owner: options.user.id,
            roll: roll,
            difficulty: options.difficulty,
            successMessage: game.i18n.format("tor2e.items.songs.chat.successMessage", {name: name }),
            song: {
                name: options.song.name,
                type: CONFIG.tor2e.songGroups[`${options.song.system.group.value}`],
            },
            actor: {
                name: name,
                avatar: options.actor.img,
            },
        };

        chatData = foundry.utils.mergeObject(chatData, {
            content: await renderTemplate("systems/tor2e/templates/chat/song-result-card.hbs", cardData),
            flags: Tor2eChatMessage.buildExtendedDataWith(cardData),
        });

        await ChatMessage.create(chatData);
    }
}