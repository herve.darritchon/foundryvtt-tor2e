import {Tor2eUndertakingDialog} from "./sheets/items/utils/Tor2eUndertakingDialog.js";

export default class Tor2eItem extends Item {

    chatTemplate = {
        "weapon": `${CONFIG.tor2e.properties.rootpath}/templates/sheets/actors/partials/character/character-weapon-card.hbs`,
        "armor": `${CONFIG.tor2e.properties.rootpath}/templates/sheets/actors/partials/character/character-armour-card.hbs`,
        "skill": `${CONFIG.tor2e.properties.rootpath}/templates/sheets/messages/partials/common/skill-roll-card.hbs`
    }

    async roll() {
        let chatData = {
            user: game.user.id,
            speaker: ChatMessage.getSpeaker()
        };

        let cardData = {
            ...this.data,
            owner: {
                id: this.actor.id,
                img: this.actor.img,
                name: this.actor.name
            }
        };

        chatData.content = await renderTemplate(this.chatTemplate[this.type], cardData);

        return ChatMessage.create(chatData);
    }

    async _preCreate(data, options, user) {
        const actor = this.parent

        if (data.type === 'standing' && game.settings.get("tor2e", "standingActivation") !== 'extended') {
            if (actor) {
                ui.notifications.warn("You can only drop a standing item in an actor with extended activation in the settings.");
            } else {
                ui.notifications.warn("You can only create a standing item with extended activation in the settings.");
            }
            return false;
        }

        if (actor && data.type === 'undertaking' && (actor.type === 'community' || actor.type === 'character')) {
            return this._tor2ePreCreateUndertaking(data, options, user);
        } else {
            return this._tor2ePreCreate(data, options, user);
        }
    }

    _tor2ePreCreate(data, options, user) {
        if (this._isNewItem()) {
            const typeToImageMap = {
                "journey-log": 'systems/tor2e/assets/images/journey-log-sample.webp',
                "armour": 'systems/tor2e/assets/images/icons/armour.png',
                "weapon": 'systems/tor2e/assets/images/icons/weapon_dagger.png',
                "undertaking": 'systems/tor2e/assets/images/icons/undertaking.webp',
                "standing": 'systems/tor2e/assets/images/icons/standing.webp',
                "skill": 'systems/tor2e/assets/images/icons/skill.png',
                "song": 'systems/tor2e/assets/images/icons/song.png',
                "fell-ability": 'systems/tor2e/assets/images/icons/adversary_fell-ability.png',
                "reward": 'systems/tor2e/assets/images/icons/reward.png',
                "miscellaneous": 'systems/tor2e/assets/images/icons/miscellaneous.webp',
                "trait": 'systems/tor2e/assets/images/icons/speciality.png',
                "virtues": 'systems/tor2e/assets/images/icons/virtue.png',
                "default": 'systems/tor2e/assets/images/icons/default.webp'
            };

            this.updateSource({img: typeToImageMap[this.type] || typeToImageMap["default"]});
        }

        return super._preCreate(data, options, user);
    }

    _isNewItem() {
        return !this.isEmbedded && !this._stats.compendiumSource && !this._stats.duplicateSource && !this._stats.createdTime;
    }

    isEquipped() {
        return this?.system?.equipped?.value === true;
    }

    async _tor2ePreCreateUndertaking(data, options, user) {
        const undertakingOptions = await Tor2eUndertakingDialog.create(data);

        if (!undertakingOptions) {
            return false;
        }

        data.system.yule.value = undertakingOptions.yule;
        data.system.free.value = undertakingOptions.free;
        data.system.location.value = undertakingOptions.location;
        data.system.season.value = undertakingOptions.season;
        data.system.year.value = undertakingOptions.year;
        data.system.usage.value = undertakingOptions.usage;
        data.system.originId.value = data._id;
        data.name = undertakingOptions.name;
        this.updateSource({
            name: data.name,
            system: data.system
        });
        return this._tor2ePreCreate(data, options, user);
    }
}
