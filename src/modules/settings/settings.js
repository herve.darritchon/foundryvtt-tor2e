import {SYSTEM} from "../system/constants.js";

export const registerSystemSettings = function () {

    /**
     * Track the system version upon which point a migration was last applied
     */
    game.settings.register(SYSTEM.id, "systemMigrationVersion", {
        name: "System Migration Version",
        scope: "world",
        config: false,
        type: String,
        default: ""
    });

    game.settings.register(SYSTEM.id, "worldKey", {
        name: "Unique world key for world tracking",
        scope: "world",
        config: false,
        type: String,
        default: "",
    })

    game.settings.register(SYSTEM.id, "devMode", {
        name: "A setting to enable or disable dev mode",
        scope: "world",
        config: false,
        type: Boolean,
        default: false,
    })

    /**
     * Track the current community actor for the company
     */
    game.settings.register(SYSTEM.id, "communityCurrentActor", {
        name: "Community Current Actor",
        scope: "world",
        config: false,
        type: String,
        default: ""
    });

    /* ******************************* CUSTOMIZABLE ******************************* */
    /**
     * Used to set the base TN Value.  In rules, propose 20 for campaign and 18 for oneshot
     */
    game.settings.register(SYSTEM.id, "tnBaseValue", {
        name: "SETTINGS.setTnBaseValue",
        hint: "SETTINGS.setTnBaseValueDescription",
        scope: "world",
        config: true,
        type: Number,
        default: 20
    });

    /**
     * Used to set the Solo Mode.
     */
    game.settings.register(SYSTEM.id, "soloMode", {
        name: "SETTINGS.setSoloMode",
        hint: "SETTINGS.setSoloModeDescription",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });

    /**
     * Used to add skirmish to standard mode (not solo).
     */
    game.settings.register(SYSTEM.id, "addSkirmish", {
        name: "SETTINGS.setAddSkirmish",
        hint: "SETTINGS.setAddSkirmishDescription",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });

    /**
     * Used to add none-custom combat special effect.
     */
    game.settings.register(SYSTEM.id, "noneCustomCombatSpecialEffect", {
        name: "SETTINGS.setNoneCustomCombatSpecialEffect",
        hint: "SETTINGS.setNoneCustomCombatSpecialEffectDescription",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });

    /**
     * Used to add skirmish to standard mode (not solo).
     */
    game.settings.register(SYSTEM.id, "pauseMessage", {
        name: "SETTINGS.setPauseMessage",
        hint: "SETTINGS.setPauseMessageDescription",
        scope: "world",
        config: true,
        type: String,
        default: ""
    });

    /**
     * Used to show/hide the skill block in the Adversary Sheet
     */
    game.settings.register(SYSTEM.id, "useAdversarySkills", {
        name: "SETTINGS.setUseAdversarySkills",
        hint: "SETTINGS.setUseAdversarySkillsDescription",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });

    /**
     * Used to override the eye awareness max value
     */
    game.settings.register(SYSTEM.id, "overridedEyeAwareness", {
        name: "SETTINGS.setOverrideEyeAwareness",
        hint: "SETTINGS.setOverrideEyeAwarenessDescription",
        scope: "world",
        config: true,
        type: Number,
        default: 24
    });

    /**
     * Used to select the raw/list mode for active effect attribute selection
     */
    game.settings.register(SYSTEM.id, "useRawModeForActiveEffect", {
        name: "SETTINGS.setRawModeForActiveEffectName",
        hint: "SETTINGS.setRawModeForActiveEffectDescription",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });

    /**
     * Used to show all the time the shield thrust special damage action to player heroes
     */
    game.settings.register(SYSTEM.id, "alwaysShowShieldThrust", {
        name: "SETTINGS.setAlwaysShowShieldThrustName",
        hint: "SETTINGS.setAlwaysShowShieldThrustDescription",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });

    /**
     * Used to show/hide warn message in the chat box for weary and miserable character
     */
    game.settings.register(SYSTEM.id, "warnLoreMasterForCharacterState", {
        name: "SETTINGS.setWarnLoreMasterForCharacterStateName",
        hint: "SETTINGS.setWarnLoreMasterForCharacterStateDescription",
        scope: "world",
        config: true,
        type: Boolean,
        default: true
    });

    /**
     * Used to show/hide the range block in the weapon item sheet and wargear block.
     */
    game.settings.register(SYSTEM.id, "showHideRangeBlock", {
        name: "SETTINGS.setShowHideRangeBlockName",
        hint: "SETTINGS.setShowHideRangeBlockDescription",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });

    /**
     * Used to show/hide a custom rules for homebrew weapon selection
     */
    game.settings.register(SYSTEM.id, "extendedWeaponSelection", {
        name: "SETTINGS.setExtendedWeaponSelection",
        hint: "SETTINGS.setExtendedWeaponSelectionDescription",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });

    /**
     * Used to disable the ALT Click protection
     */
    game.settings.register(SYSTEM.id, "disableAltClick", {
        name: "SETTINGS.disableAltClickName",
        hint: "SETTINGS.disableAltClickDescription",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });

    /**
     * Used to define the actor link data default value fpr npc
     */
    game.settings.register(SYSTEM.id, "aldDefaultValue", {
        name: "SETTINGS.aldDefaultValueName",
        hint: "SETTINGS.aldDefaultValueDescription",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });

    /**
     * Used to activate or not the display at game start of the current active Community
     */
    game.settings.register(SYSTEM.id, "displayCommunityInfoAtStart", {
        name: "SETTINGS.setDisplayCommunityInfoAtStartValue",
        hint: "SETTINGS.setDisplayCommunityInfoAtStartDescription",
        scope: "world",
        config: true,
        type: Boolean,
        default: true
    });

    /**
     * Used to modified Load as requested
     */
    game.settings.register(SYSTEM.id, "modifiedLoadAsRequested", {
        name: "SETTINGS.setModifiedLoadAsRequested",
        hint: "SETTINGS.setModifiedLoadAsRequestedDescription",
        scope: "world",
        config: true,
        type: String,
        default: ""
    });

    /**
     * Used to modified Load as requested
     */
    game.settings.register(SYSTEM.id, "inlineElementFromUuidInDescription", {
        name: "SETTINGS.setInlineElementFromUuidInDescription",
        hint: "SETTINGS.setInlineElementFromUuidInDescriptionDescription",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });

    /**
     * Used to modified Load as requested
     */
    game.settings.register(SYSTEM.id, "journeyLogCanBeModified", {
        name: "SETTINGS.setJourneyLogCanBeModified",
        hint: "SETTINGS.setJourneyLogCanBeModifiedDescription",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });

    // Standing Activation
    game.settings.register(SYSTEM.id, "standingActivation", {
        name: "SETTINGS.setStandingActivation",
        hint: "SETTINGS.setStandingActivationDescription",
        scope: "world",
        config: true,
        type: new foundry.data.fields.StringField({
            required: true,
            initial: 'None',
            choices: {
                "none": "SETTINGS.standing.none",
                "simple": "SETTINGS.standing.simple",
                "extended": "SETTINGS.standing.extended"
            }
        }),
        requiresReload: true
    });
}