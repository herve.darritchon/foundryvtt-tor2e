import Tor2eItemSheet from "./Tor2eItemSheet.js";

export default class Tor2eArmourSheet extends Tor2eItemSheet {

    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["tor2e", "sheet", "item", "armour"],
            width: 700,
            height: 500,
            tabs: [{navSelector: '.sheet-tabs', contentSelector: '.sheet-body', initial: 'details'}],
        });
    }

    /* -------------------------------------------- */

    get template() {
        return `systems/tor2e/templates/sheets/items/armour-sheet.hbs`
    }

    /* -------------------------------------------- */

    async getData() {
        const baseData = await super.getData();
        let item = baseData.item;
        let isShield = (item.system?.group?.value === CONFIG.tor2e.constants.shield);

        return foundry.utils.mergeObject(baseData, {
            isShield: isShield,
            custom: {}
        });
    }


    activateListeners(html) {
        super.activateListeners(html);
    }

}
