import {tor2eUtilities} from "../../utilities.js";
import Tor2eItemSheet from "./Tor2eItemSheet.js";

export default class Tor2eUndertakingSheet extends Tor2eItemSheet {

    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["tor2e", "sheet", "item", "undertaking"],
            width: 800,
            height: 500,
            template: `${CONFIG.tor2e.properties.rootpath}/templates/sheets/items/undertaking-sheet.hbs`,
            tabs: [{navSelector: '.sheet-tabs', contentSelector: '.sheet-body', initial: 'details'}],
        });
    }

    /* -------------------------------------------- */

    get template() {
        return `systems/tor2e/templates/sheets/items/undertaking-sheet.hbs`
    }

    /* -------------------------------------------- */

    async getData() {
        const baseData = await super.getData();
        baseData.usage = await TextEditor.enrichHTML(this.object.system?.usage?.value, {async: true});
        return baseData;
    }


    activateListeners(html) {
        super.activateListeners(html);
        html.find(".toggle").click(tor2eUtilities.eventsProcessing.onToggle.bind(this, {selector: ".editor-container"}));
    }

}
