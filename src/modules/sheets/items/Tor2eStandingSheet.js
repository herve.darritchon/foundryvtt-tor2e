import Tor2eItemSheet from "./Tor2eItemSheet.js";

export default class Tor2eStandingSheet extends Tor2eItemSheet {

    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["tor2e", "sheet", "item", "standing"],
            width: 500,
            height: 500,
            template: `${CONFIG.tor2e.properties.rootpath}/templates/sheets/items/standing-sheet.hbs`,
            tabs: [{navSelector: '.sheet-tabs', contentSelector: '.sheet-body', initial: 'details'}],
        });
    }

    /* -------------------------------------------- */

    get template() {
        return `systems/tor2e/templates/sheets/items/standing-sheet.hbs`
    }

    /* -------------------------------------------- */

    async getData() {
        return await super.getData();
    }


    activateListeners(html) {
        super.activateListeners(html);
    }

}
