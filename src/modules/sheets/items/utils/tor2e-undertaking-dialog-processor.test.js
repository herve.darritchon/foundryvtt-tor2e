const data = [
    ['name', 'HL'],
    ['location', 'R'],
    ['season', '1'],
    ['year', 2946],
    ['free', false],
    ['yule', false]
];

test('transform an array into an object', () => {
    const result = data.reduce((acc, [key, value]) => {
        acc[key] = value;
        return acc;
    }, {});
    expect(result).toStrictEqual({
        "name": "HL",
        "location": "R",
        "season": "1",
        "year": 2946,
        "free": false,
        "yule": false
    });
});