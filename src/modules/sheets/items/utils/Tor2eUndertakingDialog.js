import Tor2eUndertakingDialogProcessor from "./Tor2eUndertakingDialogProcessor.js";

export class Tor2eUndertakingDialog extends Dialog {

    /** @override
     *  @inheritdoc
     */
    constructor(data) {
        super(data);
    }

    /** @override
     *  @inheritdoc
     */
    getData(options) {
        return super.getData(options);
    }

    /** @override
     *  @inheritdoc
     */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["tor2e", "sheet", "dialog", "undertaking"],
            width: 400,
            height: "auto",
            resizable: true
        });
    }

    /** @override
     *  @inheritdoc
     */
    activateListeners(html) {
        super.activateListeners(html);
    }

    static async create(undertakingData) {
        let template = `${CONFIG.tor2e.properties.rootpath}/templates/dialog/enrich-undertaking-dialog.hbs`;
        const data = {
            undertaking: undertakingData,
            config: CONFIG.tor2e
        }
        data.backgroundImages = CONFIG.tor2e.backgroundImages["undertaking"];
        let html = await renderTemplate(template, data);

        return Dialog.wait({
            title: game.i18n.localize("tor2e.actors.community.undertakings.enrichUndertakingAndCreate"),
            content: html,
            buttons: {
                create: {
                    icon: '<i class="fa fa-check"></i>',
                    label: game.i18n.localize("tor2e.items.undertakings.messages.create"),
                    callback: (html) => {
                        const fd = new FormDataExtended(html.querySelector("form.popup.undertaking"));
                         return new Tor2eUndertakingDialogProcessor(fd).process();
                    },
                },
                cancel: {
                    icon: '<i class="fa fa-times"></i>',
                    label: game.i18n.localize("tor2e.items.undertakings.messages.cancel"),
                    callback: () => false
                },
            },
            default: "create",
            close: () => ({ confirm: false, cancelled: true })
        }, { jQuery: false, width: "auto", resizable: true, classes: ["tor2e", "sheet", "dialog", "undertaking"]});
    }

}