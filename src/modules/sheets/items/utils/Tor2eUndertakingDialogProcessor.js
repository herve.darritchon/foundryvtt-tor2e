export default class Tor2eUndertakingDialogProcessor {

    constructor(data) {
        this.data = data;
    }

    process() {
        const result = Object.entries(this.data.object).reduce((acc, [key, value]) => {
            acc[key] = value;
            return acc;
        }, {});

        result.usage = "";

        return result;
    }
}