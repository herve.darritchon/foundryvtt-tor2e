import {SYSTEM} from "./constants.js";

/**
 * Register world usage statistics
 * @param {string} registerKey
 */
function registerWorldCount(registerKey) {
    if (game.user.isGM) {
        let worldKey = game.settings.get(registerKey, "worldKey")
        if (worldKey === undefined || worldKey === "") {
            worldKey = foundry.utils.randomID(32)
            game.settings.set(registerKey, "worldKey", worldKey)
        }

        const timeZone = Intl?.DateTimeFormat()?.resolvedOptions()?.timeZone ?? "Unknown";
        const language = game?.i18n?.lang ?? "Unknown";

        // Simple API counter
        const worldData = {
            register_key: registerKey,
            world_key: worldKey,
            foundry_version: `${game.release.generation}.${game.release.build}`,
            system_name: game.system.id,
            system_version: game.system.version,
            time_zone: timeZone,
            language: language
        }

        //const apiWorlds = "http://localhost:5173";
        const apiWorlds = "https://world-tracker-git-main-hervedarritchons-projects.vercel.app";
        let apiURL = apiWorlds + "/api/worlds"
        $.ajax({
            url: apiURL,
            type: "POST",
            data: JSON.stringify(worldData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
        })
    }
}

/**
 * Show the user guide for the system if it has been updated
 * @returns {Promise<void>}
 */
async function showUserGuide() {
    if (game.user.isGM) {
        const newVer = game.system.version;
        const userGuideCompendiumName = "userguide";
        const userGuideCompendiumLabel = "System User Guide";
        const userGuideJournalName = "System User Guide";
        const userGuideVersionFlag = "UserGuideVersion";

        let currentVer = "0";
        let oldUserGuide = game.journal.getName(userGuideJournalName);
        if (oldUserGuide != null && oldUserGuide.getFlag(SYSTEM.id, userGuideVersionFlag) !== undefined) {
            currentVer = oldUserGuide.getFlag(SYSTEM.id, userGuideVersionFlag);
        }

        console.log(`TOR2E | Current version: ${currentVer} - New version: ${newVer}`);

        if (newVer === currentVer) {
            // Up to date
            return
        }

        let newReleasePack = game.packs.find((p) => p.metadata.name === userGuideCompendiumName);
        if (newReleasePack === null || newReleasePack === undefined) {
            console.info("TOR2E | No compendium found for the system guide");
            return
        }
        const releasePackIndex = await newReleasePack.getIndex();

        let newUserGuide = newReleasePack.index.find((j) => j.name === userGuideCompendiumLabel);
        if (newUserGuide === undefined || newUserGuide === null) {
            console.info("TOR2E | No system guide found in the compendium");
            return
        }

        console.log(`TOR2E | Old user guide: ${oldUserGuide} - New user guide: ${newUserGuide}`);

        // Don't delete until we have new release Pack
        if (oldUserGuide != null) {
            await oldUserGuide.delete();
        }

        await game.journal.importFromCompendium(newReleasePack, newUserGuide._id);
        let newReleaseJournal = game.journal.getName(newUserGuide.name);

        await newReleaseJournal.setFlag(SYSTEM.id, userGuideVersionFlag, newVer);

        // Show journal
        await newReleaseJournal.sheet.render(true, {sheetMode: "text"});
    }
}

// Export the functions
export {registerWorldCount, showUserGuide};