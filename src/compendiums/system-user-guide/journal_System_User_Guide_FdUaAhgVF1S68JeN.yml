folder: null
name: System User Guide
pages:
  - sort: 100000
    name: Installation and Setup
    type: text
    _id: 2Ah3PzgbzfrXs47D
    system: {}
    title:
      show: true
      level: 1
    image: {}
    text:
      format: 1
      content: >-
        <p>This User Guide assumes that you have already successfully installed
        Foundry Virtual Tabletop and the TOR2e system. It is intended for users
        who are ready to explore the system’s features and mechanics rather than
        for those looking to install or configure it for the first
        time.</p><p>If you are new to Foundry VTT or are experiencing
        difficulties with the installation of the TOR2e system, we recommend
        consulting the following resources:</p><ol><li><p><strong>Foundry VTT
        Official Documentation</strong></p><ul><li>Comprehensive guides on how
        to install, configure, and manage Foundry VTT.</li><li>Accessible from
        the <span><a rel="noopener" target="_new"
        href="https://foundryvtt.com/">Foundry VTT
        website</a></span>.</li></ul></li><li><p><strong>Community
        Resources</strong></p><ul><li>Forums, <a
        href="https://discord.com/channels/348254014598545408/348255648615628801/348256771237740545">Discord</a>
        servers, and <a
        href="http://ostolinde.free.fr/anneau_unique/aides/v2/TOR-FoundryVTT-EN.pdf">fan-created
        tutorials</a> often provide specific help for installing and
        troubleshooting community systems like TOR2e.</li><li>These resources
        can also guide you in setting up hosting options (local or
        cloud-based).</li></ul></li><li><p><strong>TOR2e System
        Page</strong></p><ul><li>Refer to the <a
        href="https://gitlab.com/herve.darritchon/foundryvtt-tor2e">system’s
        official page</a> for any additional notes or tips specific to its
        installation.</li><li>Check for updates or compatibility notes to ensure
        the system works as expected.</li></ul></li></ol><p>This guide focuses
        exclusively on how to use the TOR2e system within an already functioning
        Foundry VTT environment. If you encounter technical issues during setup,
        addressing those first will provide the smoothest experience when
        following the instructions in this guide.</p>
    video:
      controls: true
      volume: 0.5
    src: null
    ownership:
      default: -1
      Ctsaqqo45YlL21qt: 3
    flags: {}
    _stats:
      compendiumSource: null
      duplicateSource: null
      coreVersion: '12.331'
      systemId: tor2e
      systemVersion: 4.5.3
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!FdUaAhgVF1S68JeN.2Ah3PzgbzfrXs47D'
  - sort: 200000
    name: Recommended modules
    type: text
    _id: oGVgZguFWR4kLA0b
    system: {}
    title:
      show: true
      level: 2
    image: {}
    text:
      format: 1
      content: >-
        <p>To enhance your experience with the TOR2e system in Foundry VTT, the
        following modules are recommended. While not mandatory, these modules
        can significantly improve immersion, streamline gameplay, and provide
        additional customization options:</p><ol><li><p><strong>Dice So
        Nice</strong></p><ul><li><strong>Description:</strong> Adds 3D dice
        animations to Foundry VTT, allowing for visually engaging rolls. Fully
        supports the custom dice used in TOR2e (Attribute Dice, Feat Dice,
        Shadow Dice).</li><li><strong>Why Use It:</strong> Enhances immersion
        with satisfying dice roll animations and sound
        effects.</li></ul></li><li><p><strong>TOR2e Unofficial
        Compendium</strong></p><ul><li><p><strong>Description:</strong> A
        collection of pre-built assets such as adversaries, equipment, virtues,
        and rewards tailored for <em>The One Ring Second
        Edition</em>.</p></li><li><p><strong>Why Use It:</strong> Provides a
        ready-to-use library of game elements, saving time and ensuring
        consistency in your TOR2e
        campaigns.</p></li></ul></li><li><p><strong>TOR2e - Macros, Macrobar
        &amp; Communitybar</strong></p><ul><li><p><strong>Description:</strong>
        A set of pre-configured macros and an intuitive macro bar designed to
        streamline common actions in TOR2e gameplay.</p></li><li><p><strong>Why
        Use It:</strong> Simplifies repetitive tasks and provides quick access
        to essential game mechanics, enhancing efficiency and gameplay
        flow.</p></li></ul></li><li><p><strong>Tokenizer</strong></p><ul><li><p><strong>Description:</strong>
        A module for creating custom token images directly within Foundry
        VTT.</p></li><li><p><strong>Why Use It:</strong> Quickly generate
        visually consistent and high-quality tokens for characters and
        adversaries in your TOR2e games.</p></li></ul></li><li><p><strong>Simple
        Calendar</strong></p><ul><li><strong>Description:</strong> Provides an
        in-game calendar system to track time and events.</li><li><strong>Why
        Use It:</strong> Useful for managing the passage of time during long
        journeys or tracking Fellowship
        phases.</li></ul></li><li><p><strong>Journal
        Pins</strong></p><ul><li><strong>Description:</strong> Allows journals
        to be pinned directly to the map for quick access.</li><li><strong>Why
        Use It:</strong> Perfect for linking TOR2e-specific lore, quest details,
        or scene descriptions directly to locations.</li></ul></li></ol><p>These
        modules, combined with the TOR2e system, can help deliver a polished and
        immersive gameplay experience while staying true to the spirit of
        <em>The One Ring</em>. Always check for compatibility and updates when
        adding modules to your Foundry VTT installation.</p>
    video:
      controls: true
      volume: 0.5
    src: null
    ownership:
      default: -1
      Ctsaqqo45YlL21qt: 3
    flags: {}
    _stats:
      compendiumSource: null
      duplicateSource: null
      coreVersion: '12.331'
      systemId: tor2e
      systemVersion: 4.5.3
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!FdUaAhgVF1S68JeN.oGVgZguFWR4kLA0b'
  - sort: 0
    name: Introduction
    type: text
    _id: bg9pPn1h9edZQBO0
    system: {}
    title:
      show: true
      level: 1
    image: {}
    text:
      format: 1
      content: >-
        <img src="systems/tor2e/assets/artwork/tor2e-thumbnail.webp"
        /><h2>System goals and scope</h2><p>The TOR2e system for Foundry VTT is
        designed to provide a seamless and immersive experience for playing
        <em>The One Ring Second Edition</em> in a digital environment. Whether
        you're a seasoned Loremaster or a newcomer to Middle-earth, this system
        aims to faithfully recreate the unique mechanics of the tabletop game
        while leveraging the advanced features of Foundry VTT. It includes tools
        for managing characters, adversaries, and adventures, as well as
        automated dice rolls and easy-to-navigate interfaces. While the system
        is unofficial and does not include copyrighted material, it offers a
        robust foundation that you can customize with your own content to bring
        the world of Tolkien to life for your players.</p><h2>Disclaimer about
        unofficial content</h2><p>TThe TOR2e system for Foundry VTT is an
        <strong>unofficial adaptation</strong> of <em>The One Ring Second
        Edition</em>, developed independently and without affiliation with Free
        League Publishing or any official licensors. As such, this system does
        not include any copyrighted text, artwork, or proprietary material from
        the original game. Instead, it provides a framework of tools and
        features that align with the mechanics and spirit of <em>The One
        Ring</em>, allowing users to incorporate their legally owned content.
        Please ensure you use this system responsibly and in compliance with
        applicable copyright laws.</p><h2>Quick overview of features</h2><p>The
        TOR2e system for Foundry VTT offers a robust set of tools to bring
        <em>The One Ring Second Edition</em> to life in a digital space. Key
        features include:</p><ul><li><strong>Character and Adversary
        Sheets:</strong> Fully interactive sheets for creating and managing
        Player Characters (PCs), Non-Player Characters (NPCs), Adversaries, Lore
        Npcs and Community with integrated stats, equipment, and
        abilities.</li><li><strong>Custom Dice Mechanics:</strong> Automated
        rolls for Attribute and Shadow dice, complete with visual feedback, and
        compatibility with "Dice So Nice" for enhanced
        immersion.</li><li><strong>Combat:</strong> Automates combat mechanics,
        freeing the Loremaster to focus on narrating immersive and dynamic
        battle scenes.</li><li><strong>Adventure Management:</strong> Tools for
        tracking Fellowship phases and Journeys for a seamless storytelling
        experience.</li><li><strong>Hope and Shadow Management:</strong> Tools
        to track Hope, Shadow, and other character-specific resources directly
        on the sheets, ensuring smooth gameplay.</li><li><strong>and
        more</strong> …</li></ul><p>This system is designed to enhance your
        storytelling and gameplay experience while staying true to the rich
        world of Middle-earth.</p>
    video:
      controls: true
      volume: 0.5
    src: null
    ownership:
      default: -1
      Ctsaqqo45YlL21qt: 3
    flags: {}
    _stats:
      compendiumSource: JournalEntry.M2RTBu6PlLZ43kpg.JournalEntryPage.bg9pPn1h9edZQBO0
      duplicateSource: null
      coreVersion: '12.331'
      systemId: tor2e
      systemVersion: 4.5.3
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!FdUaAhgVF1S68JeN.bg9pPn1h9edZQBO0'
  - sort: 300000
    name: Initial setup for Game Masters
    type: text
    _id: yTk14wB6IAdkmCzC
    system: {}
    title:
      show: true
      level: 2
    image: {}
    text:
      format: 1
      content: >-
        <p>Before diving into your first session with the TOR2e system, a few
        essential preparations will help ensure a smooth and immersive
        experience for both you and your players. Here's how to set up your game
        environment.</p><p></p><ul><li><p><strong>@UUID[.YC8LGvAARyTRMSis]{Review
        the System
        Settings}</strong></p></li><li><p><strong>@UUID[.1l1QGtoTZ15a9hpp]{Import
        Key Compendiums}</strong></p></li></ul>
    video:
      controls: true
      volume: 0.5
    src: null
    ownership:
      default: -1
      Ctsaqqo45YlL21qt: 3
    flags: {}
    _stats:
      compendiumSource: null
      duplicateSource: null
      coreVersion: '12.331'
      systemId: tor2e
      systemVersion: 4.5.3
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!FdUaAhgVF1S68JeN.yTk14wB6IAdkmCzC'
  - sort: 400000
    name: Review the System Settings
    type: text
    _id: YC8LGvAARyTRMSis
    system: {}
    title:
      show: true
      level: 3
    image: {}
    text:
      format: 1
      content: >-
        <p>To ensure a smooth gameplay experience with the TOR2e system, it’s
        important to configure a few key settings before starting your session.
        Follow these steps:</p><ol><li><p><strong>Access the Settings
        Menu</strong></p><ul><li>Navigate to the Settings menu by clicking on
        the cogwheel icon in the top-right corner of Foundry
        VTT.</li></ul></li><li><p><strong>Set the
        Language</strong></p><ul><li>Under Foundry’s general settings, select
        your preferred language to ensure the interface matches your
        needs.</li></ul></li><li><p><strong>TOR2e System-Specific
        Options</strong></p><ul><li><p>Locate the TOR2e settings in the menu and
        configure the following options:</p><ul><li><em>Base Target Number
        (TN):</em> Choose between 18 or 20, depending on your preferred level of
        challenge for your campaign.</li><li><em>Strider Mode:</em> Enable this
        option if you are planning a solo play session to adjust the mechanics
        accordingly.</li></ul></li></ul></li><li><p><strong>Avoid Unnecessary
        Changes</strong></p><ul><li>The TOR2e system includes many advanced
        configuration options. If you are unsure about the purpose of a setting,
        it’s best to leave it as is. Experimenting without understanding the
        impact can potentially disrupt the system’s
        functionality.</li></ul></li></ol><p>By focusing on these initial
        settings, you’ll be ready to start your adventure without unnecessary
        complications. Further options will be explained in later sections of
        the User Guide as they become relevant to gameplay.</p>
    video:
      controls: true
      volume: 0.5
    src: null
    ownership:
      default: -1
      Ctsaqqo45YlL21qt: 3
    flags: {}
    _stats:
      compendiumSource: JournalEntry.K5TmWDFG1iR3tFxY.JournalEntryPage.YC8LGvAARyTRMSis
      duplicateSource: null
      coreVersion: '12.331'
      systemId: tor2e
      systemVersion: 4.5.3
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!FdUaAhgVF1S68JeN.YC8LGvAARyTRMSis'
  - sort: 500000
    name: Import Key Compendiums
    type: text
    _id: 1l1QGtoTZ15a9hpp
    system: {}
    title:
      show: true
      level: 3
    image: {}
    text:
      format: 1
      content: >-
        <div><div><div><div><p>To fully utilize the TOR2e system and any
        additional modules, it’s important to correctly activate and import
        compendiums into your world. Follow these
        steps:</p><ol><li><p><strong>Activate Installed
        Modules</strong></p><ul><li>After installing your desired modules, go to
        the Settings menu (cogwheel icon in the top-right corner), select Manage
        Modules, and activate the modules you want to use.</li><li>Ensure that
        only essential modules are activated to avoid overloading your world or
        causing potential conflicts.</li></ul></li><li><p><strong>Import
        Compendium Content</strong></p><ul><li>Some modules come with built-in
        compendiums containing pre-configured content, such as adversaries,
        equipment, or custom macros.</li><li>To use this content, open the
        Compendium Packs tab in the sidebar, locate the desired pack,
        right-click on it, and select Import All or manually drag items into
        your world as needed.</li></ul></li><li><p><strong>Avoid Overloading
        Your World</strong></p><ul><li><p>While it may be tempting to install
        numerous modules, be cautious about adding too many at once. A high
        number of active modules can:</p><ul><li>Slow down your game
        performance.</li><li>Increase the chances of bugs and module
        conflicts.</li></ul></li><li>Instead, adopt a gradual approach: install
        and activate modules only when you identify a specific need for
        them.</li></ul></li></ol><p>By carefully managing your modules and
        compendiums, you’ll maintain a clean, efficient gaming environment that
        minimizes technical issues while maximizing the TOR2e system's
        functionality.</p></div></div></div></div><div><div><div><p></p></div></div></div>
    video:
      controls: true
      volume: 0.5
    src: null
    ownership:
      default: -1
      Ctsaqqo45YlL21qt: 3
    flags: {}
    _stats:
      compendiumSource: JournalEntry.K5TmWDFG1iR3tFxY.JournalEntryPage.1l1QGtoTZ15a9hpp
      duplicateSource: null
      coreVersion: '12.331'
      systemId: tor2e
      systemVersion: 4.5.3
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!FdUaAhgVF1S68JeN.1l1QGtoTZ15a9hpp'
  - sort: -100000
    name: Releasse Notes 4.X
    type: text
    _id: BhASH7dRLS8qyv0p
    system: {}
    title:
      show: true
      level: 1
    image: {}
    text:
      format: 1
      content: >-
        <h3><strong>Release</strong> 4.5.3</h3><p><strong>Date:</strong>
        01/06/2025</p><p>Bug Fix</p><ul><li><p><strong>Health section in
        Character sheet</strong> <br />For those who plays in another language
        than english, the weary/miserable block was not translated any more for
        a while. It is back !</p></li></ul><h3><strong>Release</strong> 4.5.2
        ('<strong>First 2025'</strong> Release)</h3><p><strong>Date:</strong>
        01/05/2025</p><p>Bug Fix</p><ul><li><p><strong>Embedded Item Image
        Override</strong> <br />Resolved an issue where existing actors had
        their original images replaced by the default system image when
        duplicated or imported from compendium.</p></li></ul><h3><strong>Release
        </strong>4.5.1 (<strong>'Happy New Year'</strong>
        Release)</h3><h4><strong>Date:</strong> 1/1/2025</h4><h4>Quick
        fix</h4><ul><li>Fixed an issue where duplicate items and compendium
        items had their <strong>images overwritten</strong> by the default
        image.</li></ul><h3><strong>Release </strong>4.5.0 (PVP
        Release)</h3><h4><strong>Date:</strong> 12/27/2024</h4><h4>New
        Features</h4><ul><li>A new adversary, from page 63 of the latest <em>The
        One Ring</em> book, can cause players to turn against each
        other.</li><li><p>When an affected player attacks another player, the
        <strong>Target Number (TN)</strong> is determined by:<br />-
        <strong>Attacker’s Strength TN</strong> + <strong>Target’s Shield Parry
        Modifier (if any)</strong></p></li><li><strong>Important</strong>: The
        defender’s overall Parry score is <strong>not</strong> considered in
        these specific, spell-induced attacks.</li></ul><h3><strong>Release
        4.4.1</strong></h3><p><strong>Date:</strong> 12/26/2024</p><h4>Bug
        Fix</h4><ul><li><p><strong>Embedded Item Image Override</strong><br
        />Resolved an issue where embedded items (dragged-and-dropped into
        character sheets) had their original images replaced by the default
        system image.</p></li></ul><h3><strong>Release
        4.4.0</strong></h3><p>Version 4.4.0 of the TOR2e system delivers a host
        of exciting new features, improvements, and fixes to enhance gameplay
        and streamline the user experience. Key additions include a
        <strong>contributing document</strong> to guide contributors,
        <strong>default images</strong> for all actors and items, and a
        <strong>user guide</strong> with release notes now accessible as
        journals in Foundry VTT (displayed once at startup and available in the
        Journals thereafter). The release introduces the
        <strong>Standing</strong> feature, an optional mechanic with three
        customizable modes, and <strong>Undertaking items</strong> that can be
        dropped directly into the Community and Character sheets for a more
        intuitive workflow.</p><p>Improvements include a refined layout for
        tables in the Community sheet, ensuring better clarity and usability. A
        minor evolution also enhances the <strong>song chat message</strong> for
        a more immersive storytelling experience. Additionally, item classes
        such as armor and weapons have been refactored to leverage inheritance,
        simplifying the codebase for easier maintenance.</p><p>Version 4.4.0
        marks another step forward, equipping Loremasters and players with
        powerful tools and refined mechanics for their adventures in
        Middle-earth.</p><h3><strong>Release 4.3.0 - Singing in the
        Rain</strong></h3><p>In version 4.3.0 of the TOR2e system for Foundry
        VTT, a delightful new feature has been added to the <strong>Community
        Actor</strong>: the ability to <strong>Sing a Song</strong>! This
        feature allows players to bring the rich cultural tapestry of
        Middle-earth to life by incorporating the power of music into their
        adventures. Whether it’s a soothing melody to inspire courage during
        dark times or an ancient ballad recounting tales of old, this addition
        enhances roleplay by weaving song into the narrative. The update
        reflects the storytelling essence of <em>The One Ring</em>, where songs
        often hold deep significance and emotional resonance. As always, enjoy
        the adventure—and now, let the singing begin!</p><h3><strong>Release
        4.2.0</strong></h3><p>Version 4.2.0 of the TOR2e system introduces
        important fixes and enhancements to improve gameplay and user
        experience:</p><ul><li><strong>Hope Recovery Fix:</strong> Resolved an
        issue with Hope recovery mechanics during prolonged and short rests,
        ensuring accurate tracking and alignment with the game
        rules.</li><li><strong>Animated Background for System Tiles:</strong>
        Added a visually appealing animated background to the system tiles in
        the setup menu, enhancing the overall aesthetic and providing a polished
        interface for users.</li></ul><p>These updates not only address key
        functionality but also add a touch of style to the system setup process,
        ensuring a smoother and more engaging experience for Loremasters and
        players alike.</p><h3><strong>Release 4.1.1</strong></h3><p>Version
        4.1.1 of the TOR2e system focuses on refining gameplay mechanics and
        introducing new functionality:</p><ul><li><strong>Hope Point Spending
        Fix:</strong> Resolved bugs related to the spending of Hope points,
        ensuring smoother and more accurate gameplay during critical
        moments.</li><li><strong>New Active Effect for Load Management:</strong>
        Introduced an Active Effect (AE) to dynamically adjust a character’s
        load, allowing for more flexible and automated inventory and encumbrance
        tracking.</li></ul><p>These updates enhance the system's reliability and
        provide additional tools for managing characters' resources, offering a
        more streamlined and immersive experience for players and
        Loremasters.</p><h3><strong>Release 4.1.0</strong></h3><p>Version 4.1.0
        of the TOR2e system introduces new tools to enhance gameplay flow and
        player immersion:</p><ul><li><strong>Set Stance Macro:</strong> A
        convenient macro has been added to quickly set a character's stance
        during combat, streamlining the process and keeping the focus on the
        narrative.</li><li><strong>Journey Log Visualization:</strong> Players
        now have access to a setting and button that allows them to view the
        Journey Log, providing a shared and immersive way to track their group's
        progress during adventures.</li></ul><p>These features improve both
        combat management and exploration, making it easier for players and
        Loremasters to stay engaged in the story and mechanics of <em>The One
        Ring</em>.</p><h3><strong>Release 4.0.0</strong></h3><p>Version 4.0.0
        marks a significant milestone for the TOR2e system, introducing full
        compatibility with Foundry VTT v12. While the release notes may seem
        brief, this update required an immense amount of work behind the
        scenes.</p><p>Adapting to a major Foundry VTT version involves
        substantial code refactoring, testing, and adjustments to ensure that
        the system's features remain functional and seamless. This release
        represents countless hours of effort to maintain the stability and
        usability of the TOR2e system, ensuring that Loremasters and players can
        continue their adventures in Middle-earth without interruption. A huge
        thank you to all contributors and supporters who made this possible,
        especially my friend <a
        href="http://ostolinde.free.fr/index.php">Ghorin</a>!</p>
    video:
      controls: true
      volume: 0.5
    src: null
    ownership:
      default: -1
      Ctsaqqo45YlL21qt: 3
    flags: {}
    _stats:
      compendiumSource: JournalEntry.K5TmWDFG1iR3tFxY.JournalEntryPage.BhASH7dRLS8qyv0p
      duplicateSource: null
      coreVersion: '12.331'
      systemId: tor2e
      systemVersion: 4.5.3
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!FdUaAhgVF1S68JeN.BhASH7dRLS8qyv0p'
ownership:
  default: 0
  Ctsaqqo45YlL21qt: 3
flags:
  tor2e:
    UserGuideVersion: 4.5.3
_stats:
  compendiumSource: null
  duplicateSource: null
  coreVersion: '12.331'
  systemId: tor2e
  systemVersion: 4.5.3
  createdTime: 1736152712119
  modifiedTime: 1736152712119
  lastModifiedBy: Ctsaqqo45YlL21qt
_id: FdUaAhgVF1S68JeN
sort: 0
_key: '!journal!FdUaAhgVF1S68JeN'

