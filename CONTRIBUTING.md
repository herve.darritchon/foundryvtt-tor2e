# How to Use the System Locally

With the transition to LevelDB, there are additional steps required to create and manage packs locally. Follow these instructions to set up your environment and get started:
Setup Instructions

## Clone the Repository
Clone the TOR2e repository to your local machine as usual.

## Install Node.js
Ensure you have Node.js installed on your system. You’ll need it for managing dependencies and processing data.

## Prepare Your Environment
Before proceeding, ensure you are on the Foundry welcome page (not inside a world). Navigate to the system directory and run the following commands:

    npm install

This will generate the node_modules directory based on the package.json and package-lock.json files.

### Updating Packs

To keep your data in sync, you’ll need to unpack and repack the data as necessary. Use the following commands:

#### Unpack Data

Run  `pushLDBtoYAML` to export all actors and items from the system into YAML files in the src/packs directory. This allows you to edit the data in a human-readable format.

    npm run pushLDBtoYAML 

#### Pack Data

Run `pullYAMLtoLDB` to regenerate the packs from the updated YAML files. This step creates the .db files needed by Foundry VTT.

    npm run pullYAMLtoLDB

## Development Mode

To enable development mode and access additional debugging or testing features, run the following command in the Dev Tools console:

    game.settings.set("tor2e", "devMode", true);

Once enabled, development mode unlocks tools and behaviors tailored for testing and debugging the system locally. Remember to disable this setting in production environments to avoid unintended changes or behaviors.

These steps should help you get started with local development and pack management. If you encounter any issues, feel free to reach out for support or consult the Foundry VTT community for advice. Happy coding! 🚀